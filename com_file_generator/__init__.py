# system modules
import datetime
import getpass
import hashlib
import logging
import os
import platform
import re
import shlex
import sys
import textwrap
import urllib
from abc import ABC
from html.parser import HTMLParser

# external modules
import click
from imap_tools import MailBox
from tqdm.auto import tqdm

# internal modules
from .version import __version__

logger = logging.getLogger(__name__)


def csv_value(ctx, param, value):
    if isinstance(value, tuple) or value is None:
        return value

    try:
        return tuple(s.strip() for s in re.split(r"\s*,\s*", str(value)))
    except BaseException as e:
        raise click.BadParameter(
            f"comma-separated values expected, " f"not {value!r}: {e}"
        )


def indent(text, prefix):
    """
    Same as :any:`textwrap.indent` but also indent trailing empty lines
    """
    return re.sub(r"^", prefix, text, flags=re.MULTILINE)


def md5sum(payload):
    h = hashlib.new("md5")
    h.update(payload)
    return h.hexdigest()


class HTMLFilter(HTMLParser, ABC):
    """
    A simple no dependency HTML -> TEXT converter.
    Usage:
          str_output = HTMLFilter.convert_html_to_text(html_input)

    Taken from https://stackoverflow.com/a/62180428
    """

    def __init__(self, *args, **kwargs):
        self.text = ""
        self.in_body = False
        super().__init__(*args, **kwargs)

    def handle_starttag(self, tag: str, attrs):
        if tag.lower() == "body":
            self.in_body = True

    def handle_endtag(self, tag):
        if tag.lower() == "body":
            self.in_body = False

    def handle_data(self, data):
        if self.in_body:
            self.text += data

    @classmethod
    def convert_html_to_text(cls, html: str) -> str:
        f = cls()
        f.feed(html)
        return f.text.strip()


@click.command(
    help="Generate a Jens-style com.txt file from an email folder",
    context_settings={
        "help_option_names": ["-h", "--help"],
        "auto_envvar_prefix": "COM_FILE_GENERATOR",
    },
)
@click.option(
    "-s",
    "--server",
    help="Mail server address",
    default="mailserv.uni-tuebingen.de",
    show_default=True,
    show_envvar=True,
)
@click.option("-u", "--user", help="Mail server user", show_envvar=True)
@click.option("-p", "--password", help="Mail server password", show_envvar=True)
@click.option(
    "-f",
    "--folder",
    help="Mail folder to export, defaults to inbox",
    show_envvar=True,
)
@click.option(
    "--jens", help="Produce almost Jens-like com.txt file output", is_flag=True
)
@click.option(
    "-r",
    "--reversed",
    help="Sort from newest to oldest mail",
    default=None,
    is_flag=True,
)
@click.option(
    "--header",
    help="Header fields to show",
    default=None,
    type=click.UNPROCESSED,
    callback=csv_value,
    show_default=True,
)
@click.option(
    "--max-recipients",
    help="Limit the number of displayed recipients",
    type=int,
)
@click.option(
    "--max-messages",
    help="Limit the number of retrieved messages. Only for debugging! Doesn't obey the sorting!",
    type=int,
)
@click.option(
    "--recipients-one-line", help="Put each recipient on one line", is_flag=True
)
@click.option(
    "--date-format",
    help="strftime-style format string (e.g. '%Y-%m-%d %H:%M:%S%z for custom string formatting')",
)
@click.option(
    "--filter/--no-filter",
    "apply_filter",
    help="Whether to attempt to filter the message content",
    is_flag=True,
    default=True,
)
@click.option(
    "--show-filtered/--hide-filtered",
    help="Indicate filtered/removed parts",
    default=None,
    is_flag=True,
)
@click.option(
    "-o",
    "--output",
    help="output file",
    type=click.File("w"),
    default=sys.stdout,
)
@click.option(
    "--show-signature/--no-signature",
    help="whether to write the signature",
    default=None,
    is_flag=True,
)
@click.option(
    "--show-attachments/--no-attachments",
    help="whether to show the attachments",
    default=None,
    is_flag=True,
)
@click.option("-q", "--quiet", help="no debug output", is_flag=True)
@click.option("-v", "--verbose", help="more debug output", is_flag=True)
@click.option(
    "--show-progress/--no-progress", help="disable progress bar", is_flag=True
)
@click.version_option(version=__version__)
def cli(
    server,
    user,
    password,
    folder,
    output,
    quiet,
    verbose,
    reversed,
    show_progress,
    header,
    max_recipients,
    recipients_one_line,
    apply_filter,
    show_filtered,
    date_format,
    show_signature,
    show_attachments,
    max_messages,
    jens,
):
    cmdline = " ".join(
        map(
            shlex.quote,
            [
                (
                    os.path.basename(x)
                    if i == 0
                    else ("********" if x == password else x)
                )
                for i, x in enumerate(sys.argv)
            ],
        )
    )
    signature = f"""
🚫\tDO NOT EDIT MANUALLY! ✍️
🤖\tgenerated with {os.path.basename(sys.argv[0])} version {__version__} (https://gitlab.com/tue-umphy/software/com-file-generator) by {os.getlogin()}@{platform.node()} on {datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).strftime("%FT%T%z")} with this command:
📟\t❯ {cmdline}

↕️  sorted from {'NEWEST (⬆️  top) to OLDEST (⬇️  bottom)' if reversed else 'OLDEST (⬆️  top) to NEWEST (⬇️  bottom)'}
"""
    logging.basicConfig(
        level=logging.ERROR if quiet else (logging.DEBUG if verbose else logging.INFO)
    )
    if jens:
        # Weirdly need to do it this verbosely.
        # Neither locals() nor vars() nor exec() works 🤷
        if header is None:
            header = ("date",)
        if date_format is None:
            date_format = "%d.%m.%Y"
        if show_signature is None:
            show_signature = False
        if show_attachments is None:
            show_attachments = False
        if show_filtered is None:
            show_filtered = False
    while not server:
        server = input("Enter mail server: ")
    while not user:
        user = input("Enter user at server {server}: ")
    while not password:
        password = getpass.getpass(prompt=f"Password for {user}@{server}: ")
    # Get date, subject and body len of all emails from INBOX folder
    logger.info(f"Logging into IMAP server {server} with user {user} and password")
    with MailBox(server).login(user, password) as mailbox:
        logger.info(
            f"Successfully logged into IMAP server {server!r} "
            f"with user {user!r} and password"
        )
        if folder:
            logger.info(f"Switching folder to {folder!r}")
            mailbox.folder.set(folder)
        logger.info(f"Retrieving mails...")
        mails = sorted(
            tqdm(
                mailbox.fetch(limit=max_messages),
                total=len(mailbox.numbers()),
                unit="mail",
                disable=not show_progress,
            ),
            key=lambda x: x.date.timestamp(),
            reverse=reversed or True,
        )
        logger.info(f"Writing to {output.name}")
        if show_signature:
            output.write(f"\n{signature}\n")

        filters = {
            "drop carriage returns": lambda s: re.sub(r"\r+", r"", s),
            "drop quotes": lambda s: re.sub(
                r"^>.*$[\r\n]+", r"", s, flags=re.MULTILINE
            ),
            "drop below underlined signatures": lambda s: re.sub(
                r"^\s*(-{2,}|={2,}|-{2,}).*",
                "\n\n[🗑️ Signature ✍️]\n\n" if show_filtered else "\n",
                s,
                flags=re.MULTILINE | re.DOTALL,
            ),
            "drop base64 inline data": lambda s: re.sub(
                r"^\s*data:.*base64,.*",
                "\n\n[🗑️ base64 data 🖼️]\n\n" if show_filtered else "\n",
                s,
                flags=re.MULTILINE,
            ),
            "drop quotes aggressively": lambda s: re.sub(
                r"^\s*(?:(Am[^\r\n]+schrieb[^\r\n]+:)"
                r"|(Quoting[^\r\n]+@[^\r\n]+:)"
                r"|(-+\s*Weitergeleitete\s*Nachricht.*?))"
                r".*",
                "\n\n[🗑️ Quote 💬]\n" if show_filtered else "\n",
                s,
                flags=re.MULTILINE | re.DOTALL | re.IGNORECASE,
            ),
            "squash many empty lines": lambda s: re.sub(
                r"[\r\n]+\s*([\r\n]+\s*){2,}", "\n\n", s, flags=re.MULTILINE
            ),
            "strip leading and trailing whitespace": lambda s: s.strip(),
        }

        for msg_nr, msg in enumerate(mails):
            text = msg.text or HTMLFilter.convert_html_to_text(msg.html)
            if apply_filter:
                for filt_name, filt in filters.items():
                    text = filt((text_before := text))
                    logger.debug(
                        f"Mail {msg_nr} {msg.subject!r}: filter {filt_name!r} "
                        + (
                            f"changed text length from "
                            f"{len(text_before)} to {len(text)}"
                            if text != text_before
                            else f"didn't change anything"
                        )
                    )
            recipients_ = msg.to + msg.cc + msg.bcc
            if max_recipients and len(recipients) > max_recipients:
                recipients_ = recipients[:max_recipients] + [
                    f"(... +{len(recipients_) - max_recipients} more)"
                ]
            recipients_ = recipients_ or ["*nobody* 😶 (might be a draft...)"]
            date = msg.date.strftime(date_format) if date_format else msg.date
            header_default = dict(
                date=f"📅\tDate: {date}",
                subject=f"✍️\tSubject: {msg.subject}",
                sender=f"📧\tFrom: {msg.from_}",
                recipients="\n".join(
                    f"➡️'\tTo: {r}"
                    for r in (
                        [", ".join(recipients_)] if recipients_one_line else recipients_
                    )
                ),
            )
            if header is None:
                header = header_default
            else:
                header = {k: header_default[k] for k in header if k in header_default}
            output.write(
                "\n"
                + f"""
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╍╍╍╍╍╍╍
""".strip()
                + "\n"
            )
            if header:
                output.write(
                    indent("\n".join([""] + list(header.values()) + [""]), "┃   ")
                )
                output.write(
                    """
┠───────────────────────────────────────────────────────────────────────╌╌╌╌╌╌╌
"""
                )
            output.write(
                f"""
┃
{indent(text,'┃   ')}
""".strip()
                + "\n"
            )

            real_attachments = [
                f"📎\t{att.filename}\n\t[🗜️  {att.size} bytes 🔢  md5sum: {md5sum(att.payload)}]"
                for att in msg.attachments
                if "inline" not in att.content_disposition
                and att.filename
                and att.payload
            ]
            if real_attachments and show_attachments:
                output.write(
                    f"""
┃
┠───────────────────────────────────────────────────────────────────────╌╌╌╌╌╌╌
┃
{indent((2*chr(10)).join(real_attachments),'┃   ')}
""".strip()
                    + "\n"
                )

            output.write(
                f"""
┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╍╍╍╍╍╍╍
""".strip()
                + "\n"
            )
        if show_signature:
            output.write(f"\n{signature}\n")


if __name__ == "__main__":
    cli()
