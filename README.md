# `com.txt`-File Generator

This tool can be used to generate a Jens-style `com.txt`-file from an email folder:

## 📦 Installation

Directly from GitLab:

```bash
pip install git+https://gitlab.com/tue-umphy/software/com-file-generator.git
```

If you have [our Arch repository](https://gitlab.com/tue-umphy/workgroup-software/repository) you probably already have it installed. Otherwise:

```bash
sudo pacman -Syu python-com-file-generator
```

## 🔧 Usage

Quick-Start:

```bash
generate-com-file -s mailserv.uni-tuebingen.de -u YOUR-ZDV-LOGIN --folder Projekte/ITMS2022
# this will ask for your password...
```

This will generate output like this:

```
🚫 DO NOT EDIT MANUALLY! ✍️
🤖 generated with generate-com-file version 1.0.0+2.g34feba6 (https://gitlab.com/tue-umphy/software/com-file-generator) by yann@yann-desktop on 2022-09-28T12:46:55+0000 with this command: 
📟 ❯ generate-com-file -s mailserv.uni-tuebingen.de -u ******* -p '********' -f MyFolder



┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╍╍╍╍╍╍╍
┃
┃   📅 Date: 2022-09-28 14:46:11+02:00    
┃   ✍️ Subject: TestMail
┃   📧 From: yann-georg.buechau@uni-tuebingen.de   
┃   ➡️  To: yann-georg.buechau@uni-tuebingen.de
┃
┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╍╍╍╍╍╍╍
┃
┃   This is a test Mail!
┃   
┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╍╍╍╍╍╍╍



┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╍╍╍╍╍╍╍
┃
┃   📅 Date: 2022-09-28 12:46:35+00:00    
┃   ✍️ Subject: Another Mail!!
┃   📧 From: yann.buechau@posteo.de   
┃   ➡️  To: yann-georg.buechau@uni-tuebingen.de
┃
┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╍╍╍╍╍╍╍
┃
┃   This is another testmail!!
┃   
┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╍╍╍╍╍╍╍



🚫 DO NOT EDIT MANUALLY! ✍️
🤖 generated with generate-com-file version 1.0.0+2.g34feba6 (https://gitlab.com/tue-umphy/software/com-file-generator) by yann@yann-desktop on 2022-09-28T12:46:55+0000 with this command: 
📟 ❯ generate-com-file -s mailserv.uni-tuebingen.de -u ******* -p '********' -f MyFolder
```


### Password Handling

- Keep in mind that if you specify your password on the command-line with `-p` or `--password`, it will be stored in your shell's history. Instead, you can provide an environment variable `COM_FILE_GENERATOR_PASSWORD` with the password:

```bash
read COM_FILE_GENERATOR_PASSWORD
# ... enter your password when prompted
export COM_FILE_GENERATOR_PASSWORD
# Then the password does not need to be specified anymore
generate-com-file -s mailserv.uni-tuebingen.de -u YOUR-ZDV-USERNAME -f MyFolder
```

Otherwise, it will just ask for your password interactively.

### Saving to File

```bash
# by shell redirection
generate-com-file ... > com.txt

# with output option
generate-com-file ... -o com.txt
```

### Automation

You can automate this script e.g. with a [cron job](https://linuxhint.com/cron_jobs_complete_beginners_tutorial/), a [systemd user service](https://www.unixsysadmin.com/systemd-user-services/) or just a shell while-loop:

```bash
while true;do
  generate-com-file ... -o /path/to/com.txt
  sleep 3600  # wait some seconds until next time...
done
```
